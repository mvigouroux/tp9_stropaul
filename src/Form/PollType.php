<?php

namespace App\Form;

use App\Entity\Reponse;
use App\Entity\Sondage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PollType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('reponse', ChoiceType::class, [
                'label' => false,
                'required' => true,
                'choices' => $options['reponses'],
                //'choice_value' => 'is_check',
                'choice_label' => 'libelle',
                /*'choice_attr' => function(?Sondage $sondage) {
                    return $sondage ? ['class' => 'choice_'.strtolower($sondage->getIs_check())] : [];
                },*/
                'expanded'=>true,
                'multiple'=> $options['multiple'],
                'mapped'=>false
            ])
            ->add('Voter', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sondage::class,
            'multiple' => true,
            'reponses'=> [],
        ]);
    }
}
