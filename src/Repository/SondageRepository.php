<?php

namespace App\Repository;

use App\Entity\Sondage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @method Sondage|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sondage|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sondage[]    findAll()
 * @method Sondage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SondageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Sondage::class);
    }

    ///**
     //* @Route("/sondage/list", name="listSondages")
     //* @return Sondage[] Returns an array of Sondage objects
    //*/
    /*
   public function listSondages(EntityManagerInterface $entityManager)
   {


       return $this->createQueryBuilder('s')
           ->join('s'.id,)
           ->andWhere('s.exampleField = :val')
           ->setParameter('val', $value)
           ->orderBy('s.id', 'ASC')
           ->setMaxResults(10)
           ->getQuery()
           ->getResult()
       ;
   }
   */

    /**
     * @Route ("Sondage/{id}") name="showResults")
     * @Assert\Length( min="1", max=”255”)
     * @param EntityManagerInterface $entityManager
     * @param $id
     * @return 
     */
    public function showResults(EntityManagerInterface $entityManager, $id)
    {
        $sondageRepository= $entityManager->getRepository(Sondage::class);

        $sondage = $sondageRepository->find($id);
        $arrayResults= $sondage -> getReponse();

        return $this->render('sondage/results.html.twig', [
            'sondage' => $sondage
        ]);

    }



    /*
    public function findOneBySomeField($value): ?Sondage
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    private function render(string $string, array $array)
    {
    }
}
