<?php

namespace App\Entity;

use App\Repository\ReponseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ReponseRepository::class)
 */
class Reponse
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\NotNull
     */
    private $libelle;


    /**
     * @ORM\ManyToMany(targetEntity=User::class, mappedBy="reponse")
     */
    private $users;

    /**
     * @ORM\ManyToOne(targetEntity=Sondage::class, inversedBy="reponse")
     */
    private $sondage;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nb_check = 0;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $is_check;

    public function __construct()
    {

        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }


    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->addReponse($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            $user->removeReponse($this);
        }

        return $this;
    }

    public function getSondage(): ?Sondage
    {
        return $this->sondage;
    }

    public function setSondage(?Sondage $sondage): self
    {
        $this->sondage = $sondage;

        return $this;
    }

    public function getNbCheck(): ?int
    {
        return $this->nb_check;
    }

    public function setNbCheck(?int $nb_check): self
    {
        $this->nb_check = $nb_check;

        return $this;
    }

    public function getIsCheck(): ?int
    {
        return $this->is_check;
    }

    public function setIsCheck(?int $is_check): self
    {
        $this->is_check = $is_check;

        return $this;
    }
}
