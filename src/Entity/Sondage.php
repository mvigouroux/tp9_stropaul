<?php

namespace App\Entity;

use App\Repository\SondageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=SondageRepository::class)
 */
class Sondage
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\NotNull
     */
    private $question;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="sondage")
     * @Assert\NotBlank
     * @Assert\NotNull
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=75)
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity=Reponse::class, mappedBy="sondage", cascade={"persist"})
     * @Assert\NotBlank
     * @Assert\NotNull
     */
    private $reponse;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default": "0"})
     */
    private $nb_vote = 0;

    public function __construct()
    {
        $this->reponse = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }


    public function getQuestion(): ?string
    {
        return $this->question;
    }

    public function setQuestion(string $question): self
    {
        $this->question = $question;

        return $this;
    }


    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|Reponse[]
     */
    public function getReponse(): Collection
    {
        return $this->reponse;
    }

    public function addReponse(Reponse $reponse): self
    {
        if (!$this->reponse->contains($reponse)) {
            $this->reponse[] = $reponse;
            $reponse->setSondage($this);
        }

        return $this;
    }

    public function removeReponse(Reponse $reponse): self
    {
        if ($this->reponse->contains($reponse)) {
            $this->reponse->removeElement($reponse);
            // set the owning side to null (unless already changed)
            if ($reponse->getSondage() === $this) {
                $reponse->setSondage(null);
            }
        }

        return $this;
    }

    public function getNbVote(): ?int
    {
        return $this->nb_vote;
    }

    public function setNbVote(?int $nb_vote): self
    {
        $this->nb_vote = $nb_vote;

        return $this;
    }

    public function incrementNbVote()
    {
        $this->nb_vote = $this->nb_vote +1;

        return $this;
    }
}
