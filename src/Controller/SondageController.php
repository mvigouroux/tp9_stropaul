<?php

namespace App\Controller;

use App\Entity\Sondage;
use App\Form\PollType;
use App\Form\SondageType;
use App\Repository\SondageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/sondage")
 */
class SondageController extends AbstractController
{
    /**
     * @Route("/", name="listSondages", methods={"GET"})
     * @return Response
     */
    public function listSondages(EntityManagerInterface $entityManager): Response
    {

        $sondageRepository = $entityManager->getRepository(Sondage::class);

        $arraySondages = $sondageRepository ->findAll();

        return $this->render('sondage/index.html.twig', [
            'sondages' =>  $arraySondages,
        ]);
    }

    /**
     * @Route("/new", name="sondage_new", methods={"GET","POST"})
     */
    public function new(Request $request)
    {

        $sondage = new Sondage();
        $form = $this->createForm(SondageType::class, $sondage, [
            'attr'=>['autocomplete' => 'off'],
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($sondage);

            $entityManager->flush();

            return $this->redirectToRoute('listSondages');
        }

        return $this->render('sondage/new.html.twig', [
            'sondage' => $sondage,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sondage_show", methods={"GET"})
     */
    public function show(EntityManagerInterface $entityManager, $id, Request $request): Response
    {
        $sondageRepository = $entityManager->getRepository(Sondage::class);

        $sondage = $sondageRepository->find($id);
        $reponses = $sondage->getReponse();

        $type = $sondage->getType();
        $isMultiple = true;
        if ($type === 'unique') {
            $isMultiple = false;
        }

        $pollForm = $this->createForm(PollType::class, $sondage, [
            'method' => 'GET',
            'multiple'=> $isMultiple,
            'reponses'=> $reponses
            ]
        );


        $pollForm->handleRequest($request);
    /*
        $isCheck= $request->query->get('data')['check'];
        if($isCheck === 'true'){

        }

*/
        if ($pollForm->isSubmitted() && $pollForm->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $sondage->incrementNbVote();
            $entityManager->persist($sondage);
            $entityManager->flush();

            return $this->redirectToRoute('listSondages');
        }


        return $this->render('sondage/show.html.twig', [
            'sondage' => $sondage,
            'pollForm' => $pollForm->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="sondage_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Sondage $sondage): Response
    {
        $form = $this->createForm(SondageType::class, $sondage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('sondage_index');
        }

        return $this->render('sondage/edit.html.twig', [
            'sondage' => $sondage,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sondage_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Sondage $sondage): Response
    {
        if ($this->isCsrfTokenValid('delete'.$sondage->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($sondage);
            $entityManager->flush();
        }

        return $this->redirectToRoute('sondage_index');
    }
}
