import '../scss/app.scss';
require('@fortawesome/fontawesome-free/css/all.min.css');
require('@fortawesome/fontawesome-free/js/all.js');

//Création nouveau sondage: Générer un nouvel input libelle réponse
jQuery(document).ready(function() {


    const $collectionHolder = $('#sondage_reponse');
    const $addResponseButton = $('#newChoice');
    $collectionHolder.data('index', $collectionHolder.find('input').length);

    $addResponseButton.on('click', function(e) {
        addTagForm($collectionHolder, $addResponseButton);
    });
});

function addTagForm($collectionHolder, $newLinkLi) {
    let prototype = $collectionHolder.data('prototype');

    let index = $collectionHolder.data('index');
    let newForm = prototype;

    newForm = newForm.replace(/__name__/g, index);
    $collectionHolder.data('index', index + 1);
    let $newFormLi = $('<li></li>').append(newForm);
    $newLinkLi.before($newFormLi);
}
/*
//Ajouter un attribut sur les input réponses sélectionnées:

let type;
let choice = document.querySelector(type='checkbox')


//Afficher les résultats des sondages sous forme de graphiques: chartjs

var ctx = document.getElementById('myDoughnutChart');
var myDoughnutChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        datasets: [{
            label: '# of Votes',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: options: {
        scales: {
        yAxes: [{
            ticks: {
                beginAtZero: true
                }
            }]
        }
    }
});


*/

